package own.dnk.githubusers.data.model

data class GhUser(
    val login: String,
)