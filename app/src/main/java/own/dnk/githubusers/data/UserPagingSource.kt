package own.dnk.githubusers.data

import androidx.paging.PagingSource
import androidx.paging.PagingState
import own.dnk.githubusers.data.model.GhUser

class UserPagingSource(private val query: String,
                       private val repo: Repository): PagingSource<Int, GhUser>() {

    override fun getRefreshKey(state: PagingState<Int, GhUser>): Int = (
            (state.anchorPosition ?: 0) - state.config.initialLoadSize / 2
        ).coerceAtLeast(0)

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, GhUser> {
        return try {
            val nextPageNumber = params.key ?: 1
            val response = repo.getGitHubUsers(query, nextPageNumber)

            LoadResult.Page(
                data = response.body()?.items ?: listOf(),
                prevKey = if (params is LoadParams.Prepend) params.key else null,
                nextKey = if (params is LoadParams.Append) params.key else null
            )
        } catch (e: Exception) {
            LoadResult.Error(e)
        }
    }
}