package own.dnk.githubusers.data

import own.dnk.githubusers.data.model.GhSearchUserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface NetworkService {
    @Headers("Accept: application/vnd.github+json")
    @GET("users")
    suspend fun getGhUsers(@Query("q") userNameQuery: String, @Query("page") pageNumber: Int): Response<GhSearchUserResponse>
}