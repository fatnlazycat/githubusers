package own.dnk.githubusers.data

class Repository(retrofitService: RetrofitService) {
    private val networkService = retrofitService.getNetworkService()

    suspend fun getGitHubUsers(query: String, pageNumber: Int = 0) = networkService.getGhUsers(query, pageNumber)
}