package own.dnk.githubusers.data.model

data class GhSearchUserResponse(
    val items: List<GhUser>,
)