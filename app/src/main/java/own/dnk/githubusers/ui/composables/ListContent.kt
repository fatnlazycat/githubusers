package own.dnk.githubusers.ui.composables

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemKey
import own.dnk.githubusers.data.model.GhUser

@Composable
fun ListContent(users: LazyPagingItems<GhUser>, onItemPress: (index: Int) -> Unit) {
    LazyColumn {
        items(
            count = users.itemCount,
            key = users.itemKey(),
        ) { index ->
            val user = users[index]
            user?.let {
                UserListItem(
                    user = user,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(vertical = 15.dp)
                        .clickable(onClick = { onItemPress(index) })
                )
            }
        }
    }
}
