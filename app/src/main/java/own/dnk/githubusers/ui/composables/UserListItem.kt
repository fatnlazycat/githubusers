package own.dnk.githubusers.ui.composables

import androidx.compose.foundation.layout.Row
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import own.dnk.githubusers.data.model.GhUser

@Composable
fun UserListItem(user: GhUser, modifier: Modifier = Modifier) {
    Row(modifier = modifier) {
        Text(text = user.login)
    }
}

@Preview(showBackground = true)
@Composable
fun UserListItemPreview() {
    UserListItem(exampleUser)
}

val exampleUser = GhUser("fatnlazycat")