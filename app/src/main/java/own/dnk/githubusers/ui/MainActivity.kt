package own.dnk.githubusers.ui

import android.os.Bundle
import android.util.Log
import androidx.activity.compose.setContent
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.ModalBottomSheet
import androidx.compose.material3.SearchBar
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import androidx.paging.LoadState
import androidx.paging.compose.collectAsLazyPagingItems
import org.koin.androidx.viewmodel.ext.android.viewModel
import own.dnk.githubusers.ui.composables.ListContent

class MainActivity: FragmentActivity() {
    private val vm: MainViewModel by viewModel(clazz = MainViewModel::class)

    @OptIn(ExperimentalMaterial3Api::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val ghUsers = vm.bookPager.collectAsLazyPagingItems()
            val currentUserIndex = vm.currentUserIndexLiveData.observeAsState()

            val searchText = vm.query
            var active by rememberSaveable { mutableStateOf(false) }
            var openBottomSheet by rememberSaveable { mutableStateOf(false) }

            Column(modifier = Modifier
                .fillMaxWidth()
            ) {
                SearchBar(
                    query = searchText.value,
                    onQueryChange = { vm.setQuery(it) },
                    onSearch = {
                        active = false
                        vm.invalidateDataSource()
                    },
                    active = active,
                    onActiveChange = { active = it },
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(
                            color = MaterialTheme.colorScheme.background,
                            shape = RoundedCornerShape(8.dp)
                        )
                ) {
                    when(ghUsers.loadState.refresh) {
                        LoadState.Loading -> {
                            CircularProgressIndicator()
                        }

                        is LoadState.Error -> {
                            Text(text = "Error. Try to change the search string to continue")
                        }

                        else -> {
                            ListContent(ghUsers) { index: Int ->
                                vm.currentUserIndexLiveData.value = index
                                openBottomSheet = true
                            }
                        }
                    }

                    if (openBottomSheet) {
                        ModalBottomSheet(onDismissRequest = { openBottomSheet = false }) {
                            Column() {
                                Text(text = ghUsers[currentUserIndex.value ?: 0]?.login ?: "")
                            }
                        }
                    }
                }
            }
        }
    }
}