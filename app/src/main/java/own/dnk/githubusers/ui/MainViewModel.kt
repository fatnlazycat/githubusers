package own.dnk.githubusers.ui

import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.Pager
import androidx.paging.PagingConfig
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.debounce
import kotlinx.coroutines.flow.drop
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.onEach
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import own.dnk.githubusers.data.Repository
import own.dnk.githubusers.data.UserPagingSource
import own.dnk.githubusers.data.model.GhUser
import kotlin.coroutines.CoroutineContext

class MainViewModel : ViewModel(), KoinComponent {

    var query = mutableStateOf("")
    
    val queryForNetwork = mutableStateOf("")

    private val repo: Repository by inject()

    private var pagingSource : UserPagingSource? = null

    val bookPager = Pager(PagingConfig(pageSize = PAGE_SIZE)) {
        UserPagingSource(queryForNetwork.value, repo).also { pagingSource = it }
    }.flow

    val currentUserIndexLiveData = MutableLiveData<Int>()
    val searchTextFlow = MutableSharedFlow<String>()

    @OptIn(FlowPreview::class)
    val networkSearchFlow = searchTextFlow
        .filter { it.length > 2 }
//        .debounce(1000)
        .onEach {
            queryForNetwork.value = it
            pagingSource?.invalidate()
        }

    fun setQuery(query: String) {
        this.query.value = query
        searchTextFlow.tryEmit(query)
    }

    fun invalidateDataSource() {
        pagingSource?.invalidate()
    }

    companion object {
        const val PAGE_SIZE = 100
    }
}