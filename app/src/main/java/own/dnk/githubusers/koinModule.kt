package own.dnk.githubusers

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import own.dnk.githubusers.data.Repository
import own.dnk.githubusers.data.RetrofitService
import own.dnk.githubusers.ui.MainViewModel

val koinModule = module {
    single { RetrofitService() }
    single { Repository(get()) }

    viewModel { MainViewModel() }
}